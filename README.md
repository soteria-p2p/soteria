![logo](logo.png)
# SOTERIA - Syntropy stack enabled P2P file sharing POC

SOTERIA was the goddess or personified spirit (daimona) of safety, and deliverance and preservation from harm.

## Goal of the challenge
To set-up internal network running P2P file sharing with Syntropy Stack enabled controls and securities. To allow extra controls and network management.

## Software used

* Transmission - https://hub.docker.com/r/linuxserver/transmission
* OpenTracker - http://erdgeist.org/arts/software/opentracker/
* Tixati - https://tixati.com/
* Docker - https://docs.docker.com/get-docker/

## Master set-up
1 server is acting as initial seeder. It has OpenTracker and .torrent controller feed that allows nodes to know which files are to be downloaded.
Using external software we create a torrent file and in the tracker section we enter Opentracker running on internal IP.
Tracker is used as a peer finding mechanism in bitTorrent p2p file sharing protocol.

Note:
.torrent controller feed and Opentracker not necessarrily need to be on the same server as initial seeder for extra decompartalization they could be on separate machines.

### Running Docker

#### Transmission
```
sudo docker run -d \
  --name=transmission \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e TRANSMISSION_WEB_HOME=/combustion-release/ `#optional` \
  -p 9091:9091 \
  -p 51413:51413 \
  -p 51413:51413/udp \
  -v /opt/torrent/config:/config \
  -v /opt/torrent/downloads:/downloads \
  -v /opt/torrent/watch:/watch \
  --restart unless-stopped \
  ghcr.io/linuxserver/transmission
```

#### OpenTracker

```
sudo docker run -d --name opentracker -p 6969:6969/udp -p 6969:6969 lednerb/opentracker-docker
```
#### Torrent controller feed

```
sudo docker run -it --rm -d -p 8080:80 --name web -v /home/paulius/Projects/Hackaton/torrent-main:/usr/share/nginx/html nginx\n
```


## Node set-up

Each node has a transmission as torrent client. It was chosen due to UI provided when running on web-server environment. As well as directory listening functionalities.

### Set-up necessary folders

```
cd /opt
mkdir torrent
cd torrent
mkdir config downloads watch
```

### Running Docker
```
sudo docker run -d \
  --name=transmission \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e TRANSMISSION_WEB_HOME=/combustion-release/ `#optional` \
  -p 9091:9091 \
  -p 51413:51413 \
  -p 51413:51413/udp \
  -v /opt/torrent/config:/config `#setting up extra config directory` \
  -v /opt/torrent/downloads:/downloads `#setting up download directory` \
  -v /opt/torrent/watch:/watch `#Important - setting up torrent watch directory. When .torrent appears in this directory it will inititate download` \
  --restart unless-stopped \
  ghcr.io/linuxserver/transmission
```

### Setting up feed listener
The script goes to feed and picks a list of torrent files that are to be downloaded. We use that file to download already undownloaded torrent files to put into watch directory. Which will be picked up by transmission software
```
#!/usr/bin/env bash

cd /opt/torrent
curl -o latest_torrent_list http://172.26.0.4/ # this is IP of feed server
cd /opt/torrent/watch
wget --content-disposition --no-clobber --trust-server-names -i /opt/torrent/latest_torrent_list 
```

If connection is enabled from feed listener to node, node will pick up new torrents, otherwise it will be oblivious.


### Controlling the network

Network can be controlled using SyntropyStack UI or Syntropy Stack APIs

Auth API: https://api.syntropystack.com/api/ui/?urls.primaryName=Auth%20API
Controller API: https://api.syntropystack.com/api/ui/?urls.primaryName=Control%20Plane%20API

`/api/platform/connections/`


## Things to consider

When running multiple docker services on multiple docker machines there will inevitably be subnet clashes which will result in strange behaviour. It can be solved by specifying different subnet ranges for each machine.

It can be done by editing `/etc/docker/daemon.json`
and adding this line for `bridge` network

```
{
  "bip": "172.27.0.1/16" 
}
```
Change the above to be `172.28.0.1`, `172.29.0.1`, `172.30.0.1` and so on


## Potential future improvements

* Improve feed script installation proccess
* Create a dashboard to enable easy file adding without need of external software
### Create different filesizes files for testing

```
fallocate -l 10M test_syntropy_file.iso
fallocate -l 50M test_syntropy_file.iso
fallocate -l 200M test_syntropy_file.iso
fallocate -l 500M test_syntropy_file.iso
fallocate -l 1G test_syntropy_file.iso
fallocate -l 2G test_syntropy_file.iso
```